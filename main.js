var express = require("express");
var app = express();

var handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({defaultLayout: "main"}));
app.set("view engine", "handlebars")

var uuid = require("node-uuid");

var session = require("express-session");
app.use(session({
    secret: uuid.v1(),
    resave: false,
    saveUninitialized: false
}));

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: false}));

//Middleware
app.use(function(req, res, next) {
    //Session initialization
    if (!req.session.cart)
        req.session.cart = [];
    req.cart = req.session.cart;
    next();
});

app.get("/", function(req, res) {
    res.render("index", { 
        layout: false, 
        shoppingCart: req.cart
    });
});

app.get("/checkout", function(req, res) {
    var mycart = req.cart;
    for (var i in mycart)
        console.info(">> %i", mycart[i]);
    req.session.destroy();
    res.render("index", {
        layout: false
    });
});

app.post("/add-to-cart", function(req, res) {
    var item = req.body.item;
    console.info("item: %s", item);
    req.cart.push(item);

    res.render("index", {
        layout: false,
        shoppingCart: req.cart
    });
});

app.use(express.static(__dirname + "/public"));
app.use("/public", express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"))

app.listen(3000, function() {
    console.info("Application started on port 3000");
})